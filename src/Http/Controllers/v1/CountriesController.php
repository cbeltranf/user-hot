<?php
namespace Inmovsoftware\UserApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\UserApi\Models\V1\Country;
use Inmovsoftware\UserApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;

class CountriesController extends Controller
{
    public function index_select(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "name";
        }

        $item = Country::orderBy($sortField, $sortOrder);

        if (empty($filter) || $filter == "*") {
            $item->filterValue($filterValue);
        } else {
            $item->where($filter, 'like', "%$filterValue%");
        }


        if (empty($pageSize)) {
            $pageSize = 10;
        }

        $item->where('status', 'A');

        return new GlobalCollection($item->paginate($pageSize));
    }


    public function show(Country $country)
    {
        return response()->json($country);

    }


}
