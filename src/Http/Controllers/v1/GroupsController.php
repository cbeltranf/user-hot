<?php
namespace Inmovsoftware\UserApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\UserApi\Models\V1\Group;
use Inmovsoftware\UserApi\Models\V1\Groupuser;
use Inmovsoftware\UserApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class GroupsController extends Controller
{

    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "name";
        }

        $Auth_user = auth('api')->user();
        $groups = Group::orderBy($sortField, $sortOrder);
        $groups->where("it_business_id", "=", $Auth_user->it_business_id)->with("Groupsusers")->with("Groupsusers.User");

        return new GlobalCollection($groups->paginate($pageSize));
    }


    public function store(Request $request)
    {
       $data = $request->validate([
        "name" => "required",
        "description" => "required",
        "show_in_directory" => "required"

     ]);

     $Auth_user = auth('api')->user();

        $group = new Group;
        $group->name = $data["name"];
        $group->description = $data["description"];
        $group->status = "A";
        $group->show_in_directory = $data["show_in_directory"];
        $group->it_business_id = $Auth_user->it_business_id;
        $group->save();
        return response()->json($group);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show($group)
    {
        $group = Group::with("Groupsusers")
        ->with('Groupsusers.User')
        ->where("id", "=", $group)
        ->get();
        return response()->json($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update($group, Request $request)
    {
            $data = $request->validate([
                "name" => "required",
                "description" => "required",
                "show_in_directory" => "required",
                "status" => "nullable"
            ]);

            $Auth_user = auth('api')->user();

            $group = Group::find($group);
            $group->name = $data["name"];
            $group->description = $data["description"];
            if(isset($data["status"]) && !empty($data["status"])){
                 $group->status = $data["status"];
                }
            $group->it_business_id = $Auth_user->it_business_id;
            $group->show_in_directory = $data["show_in_directory"];
            $group->save();
            return response()->json($group);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $item = $group->delete();

        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }


    public function get_integrants(Request $request) {
        $group_id = $request->input('group_id');

    }

    public function associate(Request $request) {

        $data = $request->validate([
            "it_groups_users_id" => "required",
            "it_users_id" => "nullable"
         ]);

         DB::table('it_user_group')->where("it_groups_users_id", "=", $data["it_groups_users_id"])->delete();

         if(!empty($data["it_users_id"])){
        $custom = array();
            foreach($data["it_users_id"] as $user){
            $custom[]= array('it_groups_users_id' =>  $data["it_groups_users_id"], 'it_users_id' => $user);
            }
        DB::table('it_user_group')->insert($custom);
        }

        $items = Group::where('id', "=",   $data["it_groups_users_id"])
        ->with("Groupsusers")->with("Groupsusers.User")
        ->get();

        return response()->json($items);

    }

}
