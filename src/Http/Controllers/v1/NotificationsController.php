<?php
namespace Inmovsoftware\UserApi\Http\Controllers\V1;

use Inmovsoftware\UserApi\Http\Resources\V1\GlobalCollection;
use Inmovsoftware\UserApi\Models\V1\Userloginview as User;
use Inmovsoftware\UserApi\Models\V1\Notifications;
use Inmovsoftware\GeneralApi\Traits\V1\helper;
use Berkayk\OneSignal\OneSignalClient as One;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class NotificationsController extends Controller
{

    use helper;


    public function index(Request $request)
    {

        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";

        if (empty($sortField)) {
            $sortField = "title";
        }

        $Auth_user = auth('api')->user();

        $groups = Notifications::orderBy($sortField, $sortOrder)->where("it_business_id","=", $Auth_user->it_business_id);

        return new GlobalCollection($groups->paginate($pageSize));
    }


    public function store(Request $request)
    {

       $data = $request->validate([
        "title" => "required",
        "message" => "required",
        "from_to_value" => "required",
        "section" => "nullable",
        "notify_all" => "nullable"
     ]);

        if(!isset($data["section"]) | empty($data["section"])){
            $data["section"] = '';
        }

        $Auth_user = auth('api')->user();

        if(isset($data["notify_all"]) && $data["notify_all"] == 1){
        $users_to_notify = User::where("it_business_id", "=", $Auth_user->it_business_id)
        ->whereNotNull('uuid')->select('uuid')->get();
        $uuids = array();
        foreach($users_to_notify as $not){
            $uuids[] = $not->uuid;
                                         }
                }else{
        $item = json_decode(json_encode($data["from_to_value"]));
        $uuids = $this->users_to_notify($item->groups, $item->branch, $item->positions,  $item->users, $item->os, $type = 'uuid');

                }
        switch($data["section"]){
            case "community":
            $tt = trans('user.clicktoviewcommunity');
            break;
            case "news":
            $tt = trans('user.clicktoviewnew');
            break;
            default:
            $tt = trans('user.clicktoview');
            break;

        }
       // dd($uuids);
        $this->send_push_notification($uuids, $data["section"], '', $data["title"],  $data["message"] );

     $now = (string)Carbon::now()->format('Y-m-d H:i:s');

     $message = new Notifications;
     $message->title = $data["title"];
     $message->message = $data["message"];
     $message->from_to_value = json_encode($data["from_to_value"]);
     $message->it_business_id = $Auth_user->it_business_id;
     $message->datesend = $now;
     $message->save();

        return response()->json($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group $group
     * @return \Illuminate\Http\Response
     */
    public function show($group)
    {
        $group = Group::with("Groupsusers")
        ->with('Groupsusers.User')
        ->where("id", "=", $group)
        ->get();
        return response()->json($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group $user
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
       }


    public function get_integrants(Request $request) {
        $group_id = $request->input('group_id');

    }

    public function associate(Request $request) {

        $data = $request->validate([
            "it_groups_users_id" => "required",
            "it_users_id" => "required"
         ]);

         DB::table('it_user_group')->where("it_groups_users_id", "=", $data["it_groups_users_id"])->delete();

        $custom = array();
        foreach($data["it_users_id"] as $user){
         $custom[]= array('it_groups_users_id' =>  $data["it_groups_users_id"], 'it_users_id' => $user);
        }

        DB::table('it_user_group')->insert($custom);

        $items = Group::where('id', "=",   $data["it_groups_users_id"])
        ->with("Groupsusers")->with("Groupsusers.User")
        ->get();

        return response()->json($items);

    }

}
