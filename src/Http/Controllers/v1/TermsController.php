<?php
namespace Inmovsoftware\UserApi\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Inmovsoftware\UserApi\Models\V1\Terms;
use Inmovsoftware\UserApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class TermsController extends Controller
{

    public function index(Request $request)
    {


    }


    public function store(Request $request)
    {


    }

    public function show($terms)
    {

    }

    public function update($terms, Request $request)
    {


    }


    public function destroy($terms)
    {

    }


    public function get_active()
    {
            $item = Terms::where("status", "=", "A")->get();

            return response()->json($item);
    }

    public function check_accept()
    {
            $Auth_user = auth('api')->user();
            if(isset($Auth_user->id) && !empty($Auth_user->id)){
                $item['accept'] = DB::select("SELECT  f_check_accept_terms(".$Auth_user->id.")  AS accept")[0]->accept;
                 return response()->json($item);
            }else{
                    return response()->json(
                    [
                            'errors' => [
                            'status' => 401,
                                'messages' => [trans('user.usersessionnotfound')]
                                ]
                            ],
                        401
                    );

            }

    }

    public function accept_again(){

        $Auth_user = auth('api')->user();
        $item = Terms::where("status", "=", "A")->get();

        $item['accept'] = DB::select("SELECT  f_check_accept_terms(".$Auth_user->id.")  AS accept")[0]->accept;

        if( $item['accept'] == 0 ){
            DB::table('it_user_term_condition_policy')->insert(
                ['it_terms_conditions_policies_id' => $item[0]->id, 'it_business_id' => $Auth_user->it_business_id, 'it_users_id' => $Auth_user->id]
            );

            $code = 200;
            $message = 'ok';

        }else{
            $code = 200;
            $message = 'ya aceptado';
        }

            return response()->json(
                [
                    'errors' => [
                        'status' => $code,
                        'messages' => [ $message ]
                    ]
                ],
                $code
            );

    }


}
