<?php
namespace Inmovsoftware\UserApi\Http\Controllers\V1;

use Inmovsoftware\UserApi\Http\Resources\V1\GlobalCollection;
use Inmovsoftware\PositionApi\Models\V1\Position;
use Inmovsoftware\CompanyApi\Models\V1\Branches;
use Inmovsoftware\ProfileApi\Models\V1\Profile;
use Inmovsoftware\GeneralApi\Traits\V1\helper;
use Berkayk\OneSignal\OneSignalClient as One;
use Inmovsoftware\UserApi\Models\V1\Country;
use Inmovsoftware\UserApi\Models\V1\User;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Excel;
use DB;
/* Se agregan los modelos y recursos requeridos en el controlador */



class UserController extends Controller
{
    /**
     * Se incluye el Trait helper desde el package general
     */
    use helper;

    public function index(Request $request)
    {

        /**
         * Se reciben los parametros necesarios para filtrar el listado
         */
        $filter = $request->input("filterColumn");
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortField = $request->input("sortField");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sede = $request->input("branch");

        if (empty($sortField)) {
            $sortField = "it_users.name";
        }

        /**
         * Se obtiene la información del usuario en sesíon
         */
        $Auth_user = auth('api')->user();

        if (isset($Auth_user->id) && !empty($Auth_user->id)) {


            /**
             * Se construye la petición al modelo tomando las distintas opciones de filtrado
             */
            $user = User::orderBy('it_users.' . $sortField, $sortOrder);


            if (empty($filter) || $filter == "*") {
                /**
                 * En el modelo se creó un Scope llamado filterValue, que permite buscar una cadena en distintos campos al mismo tiempo
                 */
                $user->filterValue($filterValue);
            } else {
                $user->where($filter, 'like', "%$filterValue%");
            }


            /**
             * En el modelo se creó un Scope llamado selectAllItems, que permite seleccionar solo los campos requeridos en el front
             * Teniendo en cuenta que la petición contendría demasidos datos basura por tratarse de al menos 4 tablas involucradas.
             */
            $user->selectAllItems();


            if (empty($pageSize)) {
                $pageSize = 10;
            }

            /**
             * Se obtienen las sedes de la empresa correspondiente al usuario en sesión
             */
            $my_branch = DB::table('vs_login as l')
                ->where('l.id', '=', $Auth_user->id)
                ->join('it_branches as b', 'b.it_business_id', '=', 'l.it_business_id')
                ->select('b.id')
                ->get();

            $in = array();
            foreach ($my_branch as $item) {
                $in[] = $item->id;
            }
            $user->whereIn('it_branches_id', $in);

            if (!empty($sede)) {
                $user->where('it_users.it_branches_id', '=', $sede);
            }

            /**
             * Se envía el resultado al GlobalCollection con su respectiva paginación.
             */
            return new GlobalCollection($user->paginate($pageSize));
        } else {
            /**
             * Si el token del usuario en sesión es inválido se procede a generar la exepción correspondiente
             */
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.token_invalid')]
                    ]
                ],
                401
            );
        }
    }

    public function store(Request $request)
    {
        /**
         * Se reciben los parametros a ser validados
         */
        $data = $request->validate([
            "name" => "required|max:50",
            "last_name" => "required|max:50",
            "birthday" => "nullable|max:15",
            "password" => "nullable|max:50",
            "country_iso" => "required|exists:it_countries,iso",
            "email" => "email|unique:it_users",
            "groups" => "nullable"
        ]);

        $iso = $data["country_iso"];
        $group_ = $data["groups"];

        $data = request()->only(
            "name",
            "last_name",
            "birthday",
            "password",
            "it_branches_id",
            "can_notify",
            "it_positions_id",
            "it_profile_id",
            "phone",
            "email",
            "login_alt",
            "session_type"
        );


        /**
         * Se eliminan los parametros que vienen en vacío puesto que desde la app no se actualiza la misma información
         * que en el panel y si se pasa un parametro vacio se puede eliminar información accidentalmente.
         */
        $data = array_filter($data);

        /**
         * Se unifica el formato de los nombres y apellidos
         */
        $data["name"] = ucwords(strtolower($data["name"]));
        $data["last_name"] = ucwords(strtolower($data["last_name"]));

        /**
         * Se utiliza el método check_free_email() del Trait helper;
         */
        if ($this->check_free_email($data["email"])) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 500,
                        'messages' => [trans('user.freemail')]
                    ]
                ],
                401
            );
        }

        /**
         * Se obtiene la información del usuario en sesíon
         */
        $Auth_user = auth('api')->user();
        if (isset($Auth_user->id) && !empty($Auth_user->id)) {

            $company_mail = $Auth_user->email;
            $sub = explode('@', $company_mail);
            $sub = explode('.', $sub[1]);

            $em = explode('@', $data["email"]);
            $em = explode('.', $em[1]);

            if ($sub[0] !=  $em[0]) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 500,
                            'messages' => [trans('user.noemailenterprise')]
                        ]
                    ],
                    500
                );
            }


            if (!empty($data["password"])) {
                $data["password"] = bcrypt($data["password"]);
            } else {
                $data["password"] = bcrypt("12345678");
            }


            $get_country = $this->get_country_from_iso($iso);
            $data["it_countries_id"] = $get_country;


            $user = User::firstOrNew($data);
            $user->save();
            $user->Country;
            $user->Position;
            $user->Profile;

            foreach ($group_  as $k => $v) {
                DB::table('it_user_group')->insert([
                    ['it_users_id' => $user->id, 'it_groups_users_id' => $v]
                ]);
            }

            $return["user"] = $user;
            return response()->json($user);
        } else {
            /**
             * Si el token del usuario en sesión es inválido se procede a generar la exepción correspondiente
             */
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.token_invalid')]
                    ]
                ],
                401
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        /**
         * Se obtiene el id del usuario a eliminarse proveniente de la url
         * Y laravel automáticamente aplica un find al modelo devolviendo la instacia del usuario a mostrarse
         * Posteriormente se hace uso de las relaciones entre modelos para obtener toda la información necesaria
         */

        /**
         * Adicionalmente la eliminacion del usuario no se permitirá si dicho usuario es el unico administrador de la compañia
         * Se realiza la validación correspondiente
         */
        $profile = Profile::where("it_business_id", $user->it_business_id)->where("level", '1')->select("id")->get();

        $stack = array();
        foreach ($profile as $item) {
            array_push($stack, $item->id);
        }

        $cant_admin = User::join('it_branches', 'it_branches.id', '=', 'it_users.it_branches_id')
            ->join('it_business', 'it_business.id', '=', 'it_branches.it_business_id')
            ->where('it_users.status', '=', 'A')
            ->where('it_users.status', '=', 'A')
            ->where('it_branches.id', '=', $user->it_branches_id)
            ->whereIn('it_users.it_profile_id', $stack)
            ->whereNull('it_users.deleted_at')
            ->count();



        if ($cant_admin <= 1 && in_array($user->it_profile_id,  $stack)) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 500,
                        'messages' => [trans('user.noadminsleftdelete')]
                    ]
                ],
                500
            );
        }



        $item = $user->delete();


        $response["deleted"] = $item;
        if ($item) {
            $response["status"] = 200;
        } else {
            $response["status"] = 401;
        }

        return response()->json($response);
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        /**
         * Se obtiene el id del usuario a mostrarse proveniente de la url
         * Y laravel automáticamente aplica un find al modelo devolviendo la instacia del usuario a mostrarse
         * Posteriormente se hace uso de las relaciones entre modelos para obtener toda la información necesaria
         */

        $user->Position;
        $user->Profile;
        $user->Country;

        /**
         * Se consulta de forma manual la información de la compañia
         */
        $user->company = DB::table('it_business as c')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $user->it_branches_id)
            ->select('c.id as id', 'c.alias as alias', 'c.logo as logo', 'c.name as name')
            ->get();

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {
        /**
         * Se reciben los parametros a ser validados
         */
        $data = $request->validate([
            "name" => "required|max:255",
            "last_name" => "required|max:255",
            "phone" => "nullable",
            "birthday" => "nullable|max:15",
            "position" => "nullable",
            "it_positions_id" => "nullable",
            "it_profile_id" => "nullable",
            "session_type" => "nullable",
            "can_notify" => "nullable",
            "it_branches_id" => "nullable",
            "password" => "nullable|max:50",
            "email" => "required|email",
            "country_iso" => "required|exists:it_countries,iso",
            "groups" => "nullable"
        ]);

        $iso = $data["country_iso"];

        $plat = $request->header('X-Platform');

        $user = User::find($user);
        if ($plat != 'mobile') {
            if (isset($data["groups"])) {
                $group_ = $data["groups"];
                DB::table('it_user_group')->where('it_users_id', '=', $user->id)->delete();
                foreach ($group_ as $k => $v) {
                    DB::table('it_user_group')->insert([
                        ['it_users_id' => $user->id, 'it_groups_users_id' => $v]
                    ]);
                }
            }
        }

        $old_mail = $user->email;
        $sub = explode('@', $old_mail);
        $sub = explode('.', $sub[1]);
        $phone = $data["phone"];

        $data = request()->only("name", "last_name",  "phone", "birthday",  "it_positions_id", "it_profile_id", "session_type", "can_notify", "it_branches_id", "password", "email", "groups");
        $pos =  request()->only("position");


        if (!isset($data['it_branches_id']) || empty($data['it_branches_id'])) {
            $data['it_branches_id'] = $user->it_branches_id;
        }

        if (!empty($pos)) {
            $data['it_positions_id'] = $pos["position"];
        }


        /**
         * Se eliminan los parametros que vienen en vacío puesto que desde la app no se actualiza la misma información
         * que en el panel y si se pasa un parametro vacio se puede eliminar información accidentalmente.
         */
        $data = array_filter($data);


        $data['phone'] = $phone;
        $data['update_data'] = 1;

        if (!empty($data["password"])) {
            $data["password"] = bcrypt($data["password"]);

            DB::table('it_users')
                ->where('id', "=", $user->id)
                ->update(['password' => $data["password"]]);
        }

        if (!empty($data["email"])) {
            $em = explode('@', $data["email"]);
            $em = explode('.', $em[1]);


            if ($sub[0] !=  $em[0]) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 500,
                            'messages' => [trans('user.noemailenterprise')]
                        ]
                    ],
                    500
                );
            }


            $profile = Profile::where("it_business_id", $user->it_business_id)->where("level", '1')->select("id")->get();
            $stack = array();
            foreach ($profile as $item) {
                array_push($stack, $item->id);
            }


            $cant_admin = User::join('it_branches', 'it_branches.id', '=', 'it_users.it_branches_id')
                ->join('it_business', 'it_business.id', '=', 'it_branches.it_business_id')
                ->where('it_users.status', '=', 'A')
                ->where('it_users.status', '=', 'A')
                ->where('it_branches.id', '=', $data["it_branches_id"])
                ->whereIn('it_users.it_profile_id', $stack)
                ->whereNull('it_users.deleted_at')
                ->count();


            /**
             * No se permitirá el cambio de perfil si el usuario es el unico administrador de la compañia
             * Se realiza la validación correspondiente
             */
            if ($cant_admin <= 1 && in_array($data['it_profile_id'], $stack)) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 500,
                            'messages' => [trans('user.noadminsleft')]
                        ]
                    ],
                    500
                );
            }

            /**
             * Se verifica que el Email sea único
             */
            $unique_email = DB::table('it_users')
                ->where('id', "!=", $user->id)
                ->where('email', "=", $data["email"])
                ->count();

            if ($unique_email > 0) {
                return response()->json(
                    [
                        'errors' => [
                            'status' => 409,
                            'messages' => [trans('user.emailtaken')]
                        ]
                    ],
                    409
                );
            }
        }

        $get_country = $this->get_country_from_iso($iso);
        $data["it_countries_id"] = $get_country;

        $data["name"] = ucwords(strtolower($data["name"]));
        $data["last_name"] = ucwords(strtolower($data["last_name"]));

        /**
         * Se actualiza la información del usuario y se prepara la respuesta para refrescar los modelos en el front
         */
        $user->update($data);
        $user->Position;
        $user->Profile;
        $user->Country;



        $return["user"] = $user;

        $return["company"] = DB::table('it_business as c')
            ->join('it_branches as b', 'c.id', '=', 'b.it_business_id')
            ->where('b.id', '=', $user->it_branches_id)
            ->select('c.id as id', 'c.alias as alias', 'c.logo as logo', 'c.name as name')
            ->get();

        $return["position"] = DB::table('it_positions')
            ->where('id', '=', $user->it_positions_id)
            ->select('id', 'name')
            ->get();

        $return["profile"] = DB::table('it_positions')
            ->where('id', '=', $user->it_positions_id)
            ->select('id', 'name')
            ->get();

        $return["card_url"] = $this->make_bitly_url("http://cgi.getdc.us/?user=" . $user->id);

        return response()->json($return);
    }


    public function active_inactive($id)
    {
        /**
         * Método destinado para la activación o inactivación de los usuarios del sistema
         */

        $user = User::find($id);
        if ($user->status == "A") {
            $new_state = "C";
            $user->status = $new_state;
        } else {
            $new_state = "A";
            $user->status = $new_state;
        }
        $user->save();

        return response()->json($new_state);
    }


    public function mass_store(Request $request)
    {

        $data = $request->validate([
            "file" => "required",
        ]);

        $Auth_user = auth('api')->user();

        $code = '';
        $result = '';
        $route_txt = '';

        Excel::load($request->file('file')->getRealPath(), function ($reader) use ($data, &$Auth_user, &$code, &$return_value, &$route_txt) {
            $reader->skipRows(0);
            $hojas = $reader->get()->toArray();


            //DB::beginTransaction();
            $positions_no_exists = array();
            $branches_no_exists = array();
            $can_notify_no_exists = array();
            $session_no_exists = array();
            $profile_no_exists = array();
            $country_no_exists = array();
            $email_duplicated = array();
            $emails_free = array();
            $email_haystack = array();
            $email_allready_exist = array();
            $email_dont_belongs = array();

            $phone_haystack = array();
            $phone_allready_exist = array();
            $phone_duplicated = array();
            $c = 0;
            $crl = 0;
            $content_report = '';

            $user = array();
            foreach ($hojas[0] as $filas) {
                if (!empty(trim($filas["name"])) && !empty(trim($filas["email"]))) {

                    $birth = new Carbon($filas["birthday"]);

                    $filas = array_map('trim', $filas);
                    $user[$c]['name'] = $filas["name"];
                    $user[$c]['last_name'] = $filas["last_name"];
                    $user[$c]['birthday'] = $birth->format('Y-m-d');
                    $user[$c]['phone'] = $filas["phone"];
                    $user[$c]['email'] = $filas["email"];

                    if (!empty($filas["country"])) {
                        $check = Country::where("name", "=", $filas["country"])->orWhere("nombre", "=", $filas["country"])->get();
                        if ($check->count() < 1) {
                            $country_no_exists[] = $filas["country"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.masscontry') . ': ' . $filas["country"] . "\n";
                        } else {
                            $user[$c]['it_countries_id'] = $check[0]->id;
                            $user[$c]['country_phone'] = '+' . $check[0]->phonecode;
                        }
                    } else {
                        $country_no_exists[] = trans('user.cellempty');
                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.masscontry') . ': ' . trans('user.cellempty') . "\n";
                    }



                    if (!empty($filas["branch"])) {
                        $check = \Inmovsoftware\CompanyApi\Models\V1\Branches::where("name", "=",  $filas["branch"])->where("it_business_id", "=", $Auth_user->it_business_id)->get();
                        if ($check->count() < 1) {
                            $branches_no_exists[] = $filas["branch"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massbranches') . ': ' . $filas["branch"] . "\n";
                        } else {
                            $user[$c]['it_branches_id'] = $check[0]->id;
                        }
                    } else {
                        $branches_no_exists[] = trans('user.cellempty');

                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massbranches') . ': ' . trans('user.cellempty') . "\n";
                    }

                    if (!empty($filas["position"])) {
                        $check = \Inmovsoftware\PositionApi\Models\V1\Position::where("name", "=",  $filas["position"])->where("it_business_id", "=", $Auth_user->it_business_id)->get();
                        if ($check->count() < 1) {
                            $positions_no_exists[] = $filas["position"];

                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.masspositions') . ': ' . $filas["position"] . "\n";
                        } else {
                            $user[$c]['it_positions_id'] = $check[0]->id;
                        }
                    } else {
                        $positions_no_exists[] = trans('user.cellempty');

                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.masspositions') . ': ' . trans('user.cellempty') . "\n";
                    }

                    if (!empty($filas["profile"])) {
                        $user[$c]['it_profile_id'] = $filas["profile"];
                        $check = Profile::where("name", "=",  $filas["profile"])->get();
                        if ($check->count() < 1) {
                            $profile_no_exists[] = $filas["profile"];

                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massprofile') . ': ' . $filas["profile"] . "\n";
                        } else {
                            $user[$c]['it_profile_id'] = $check[0]->id;
                        }
                    } else {
                        $profile_no_exists[] = trans('user.cellempty');

                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massprofile') . ': ' . trans('user.cellempty') . "\n";
                    }


                    if (!empty($filas["email"])) {
                        $check = User::where("email", "=",  $filas["email"])->count();

                        if ($this->check_free_email($filas["email"])) {
                            $emails_free[] = $filas["email"];

                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.freemail') . ': ' . $filas["email"] . "\n";
                        }



                        $company_mail = $Auth_user->email;
                        $sub = explode('@', $company_mail);
                        $sub = explode('.', $sub[1]);

                        $em = explode('@', $filas["email"]);
                        $em = explode('.', $em[1]);

                        if ($sub[0] !=  $em[0]) {
                            $email_dont_belongs[] = $filas["email"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massemaildontbelongs') . ': ' . $filas["email"] . "\n";
                        }

                        $unique_email = DB::table('it_users')
                            ->where('email', "=", $filas["email"])
                            ->count();

                        if ($unique_email > 0) {
                            $email_allready_exist[] = $filas["email"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massemaildb') . ': ' . $filas["email"] . "\n";
                        }


                        if ($check < 1 && !in_array($filas["email"], $email_haystack)) {
                            $email_haystack[] = $filas["email"];
                            $user[$c]['email'] = $filas["email"];
                            $user[$c]['login_alt'] = ''; //str_replace(".", '', str_replace("@", "", $filas["email"]));
                        } else {
                            $email_duplicated[] = $filas["email"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massemaildup') . ': ' . $filas["email"] . "\n";
                        }
                    } else {
                        $email_duplicated[] = trans('user.cellempty');
                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massemaildup') . ': ' . trans('user.cellempty') . "\n";
                    }



                    if (!empty($filas["phone"])) {

                        $unique_phone = DB::table('it_users')
                            ->where('phone', "=", $filas["phone"])
                            ->count();

                        if ($unique_phone > 0) {
                            $phone_allready_exist[] = $filas["phone"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massphonedb') . ': ' . $filas["phone"] . "\n";
                        }

                        $check = User::where("phone", "=",  $filas["phone"])->count();
                        if ($check < 1 & !in_array($filas["phone"], $phone_haystack)) {
                            $phone_haystack[] = $filas["phone"];
                            $user[$c]['phone'] = $filas["phone"];
                        } else {
                            $phone_duplicated[] = $filas["phone"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massphonedup') . ': ' . $filas["phone"] . "\n";
                        }
                    }

                    if (!empty($filas["session_type"])) {
                        if (in_array($filas["session_type"], array("M", "S"))) {
                            $user[$c]['session_type'] = $filas["session_type"];
                        } else {
                            $session_no_exists[] = $filas["session_type"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.masssession') . ': ' . $filas["session_type"] . "\n";
                        }
                    } else {
                        $session_no_exists[] = trans('user.cellempty');
                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.masssession') . ': ' . trans('user.cellempty') . "\n";
                    }


                    if (!empty($filas["can_notify"])) {
                        $filas["can_notify"] = strtolower(trim($filas["can_notify"]));
                        $user[$c]['can_notify'] = strtolower($filas["can_notify"]);

                        if (in_array($filas["can_notify"], array("y", "n"))) {
                            if ($filas["can_notify"] == 'y') {
                                $filas["can_notify"] = 'S';
                            } else {
                                $filas["can_notify"] = 'N';
                            }
                            $user[$c]['can_notify'] = $filas["can_notify"];
                        } else {
                            $can_notify_no_exists[] = $filas["can_notify"];
                            $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massnotify') . ': ' . $filas["can_notify"] . "\n";
                        }
                    } else {
                        $can_notify_no_exists[] = trans('user.cellempty');
                        $content_report .= trans('user.line') . ' ' . ($crl + 2) . ' - ' . trans('user.massnotify') . ': ' . trans('user.cellempty') . "\n";
                    }


                    $c++;
                }
                $crl++;


            }


            if (
                empty($positions_no_exists) &&
                empty($branches_no_exists) &&
                empty($can_notify_no_exists) &&
                empty($session_no_exists) &&
                empty($profile_no_exists)  &&
                empty($email_duplicated)  &&
                empty($email_allready_exist)  &&
                empty($phone_duplicated)  &&
                empty($phone_allready_exist)  &&
                empty($email_dont_belongs)  &&
                empty($emails_free)
            ) {
                // DB::commit();

                $return_value = trans('user.massok');
                $code = 200;
                // dd($user);
                User::insert($user);
            } else {

                // DB::rollBack();

                $error_detail = '';

                if (!empty($email_duplicated)) {
                    $email_duplicated = array_unique($email_duplicated);
                    sort($email_duplicated, SORT_NUMERIC);

                    $error_detail .= '<li>' . trans('user.massemaildup') . ': ' . json_encode($email_duplicated) . ' </li>';
                }


                if (!empty($email_allready_exist)) {
                    $email_allready_exist = array_unique($email_allready_exist);
                    sort($email_allready_exist, SORT_NUMERIC);

                    $error_detail .= '<li>' . trans('user.massemaildb') . ': ' . json_encode($email_allready_exist) . ' </li>';
                }

                if (!empty($phone_duplicated)) {
                    $phone_duplicated = array_unique($phone_duplicated);
                    sort($phone_duplicated, SORT_NUMERIC);

                    $error_detail .= '<li>' . trans('user.massphonedup') . ': ' . json_encode($phone_duplicated) . ' </li>';
                }


                if (!empty($phone_allready_exist)) {
                    $phone_allready_exist = array_unique($phone_allready_exist);
                    sort($phone_allready_exist, SORT_NUMERIC);

                    $error_detail .= '<li>' . trans('user.massphonedb') . ': ' . json_encode($phone_allready_exist) . ' </li>';
                }


                if (!empty($email_dont_belongs)) {
                    $email_dont_belongs = array_unique($email_dont_belongs);
                    sort($email_dont_belongs, SORT_NUMERIC);

                    $error_detail .= '<li>' . trans('user.massemaildontbelongs') . ': ' . json_encode($email_dont_belongs) . ' </li>';
                }



                if (!empty($emails_free)) {
                    $emails_free = array_unique($emails_free);
                    sort($emails_free, SORT_NUMERIC);

                    $error_detail .= '<li>' . trans('user.freemail') . ': ' . json_encode($emails_free) . ' </li>';
                }

                if (!empty($positions_no_exists)) {
                    $positions_no_exists = array_unique($positions_no_exists);
                    sort($positions_no_exists, SORT_NUMERIC);
                    $error_detail .= '<li>' . trans('user.masspositions') . ': ' . json_encode($positions_no_exists) . ' </li>';
                }

                if (!empty($branches_no_exists)) {
                    $branches_no_exists = array_unique($branches_no_exists);
                    sort($branches_no_exists, SORT_NUMERIC);
                    $error_detail .= '<li>' . trans('user.massbranches') . ': ' . json_encode($branches_no_exists) . ' </li>';
                }

                if (!empty($profile_no_exists)) {
                    $profile_no_exists = array_unique($profile_no_exists);
                    sort($profile_no_exists, SORT_NUMERIC);
                    $error_detail .= '<li>' . trans('user.massprofile') . ': ' . json_encode($profile_no_exists) . ' </li>';
                }

                if (!empty($can_notify_no_exists)) {
                    $positions_no_exists = array_unique($can_notify_no_exists);
                    sort($positions_no_exists, SORT_NUMERIC);
                    $error_detail .= '<li>' . trans('user.massnotify') . ': ' . json_encode($can_notify_no_exists) . ' </li>';
                }

                if (!empty($session_no_exists)) {
                    $session_no_exists = array_unique($session_no_exists);
                    sort($session_no_exists, SORT_NUMERIC);
                    $error_detail .= '<li>' . trans('user.masssession') . ': ' . json_encode($session_no_exists) . ' </li>';
                }


                $return_value =  trans('user.masserrors') . '<ul>' . $error_detail . '</ul>';
                $code = 500;

                $route_txt = 'reports/file' . time() . '.txt';
                Storage::disk('public')->put($route_txt, $content_report);
            }
        });


        return response()->json(
            [
                'errors' => [
                    'status' => $code,
                    'messages' => [$return_value]
                ],
                'link' => $route_txt
            ],
            $code
        );
    }


    public function generate_custom_excel($id)
    {
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();

        $spreadsheet->getProperties()->setCreator('My Digital Card - InmovTech - INMOV SAS')
            ->setLastModifiedBy('InmovTech')
            ->setTitle('Upload Users Format')
            ->setSubject('Office 2007 XLSX - Upload Users Format')
            ->setDescription('Upload Users Format In Office 2007 XLSX, for My Digital Card Solution.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Upload Users Format');


        $profiles = Profile::where("status", "=", "A")->where("it_business_id", "=", $id)->select("name")->get();
        $branches = Branches::where("status", "=", "A")->where("it_business_id", "=", $id)->select("name")->get(); //
        $position = Position::where("status", "=", "A")->where("it_business_id", "=", $id)->select("name")->get(); //->where("it_business_id", "=", $id)
        $countryCodes = Country::where("status", "=", "A")->select(trans('user.name'))->orderByRaw('id = 228 DESC, id = 47 DESC, ' . trans('user.name') . ' ASC')->get();


        $worksheet1 = $spreadsheet->createSheet();
        $worksheet1->setTitle('Sheet1');
        $spreadsheet->setActiveSheetIndex(1);
        $spreadsheet->getActiveSheet()
            ->setSheetState(\PhpOffice\PhpSpreadsheet\Worksheet\Worksheet::SHEETSTATE_HIDDEN);

        $count_countryCodes = 0;
        foreach ($countryCodes as $item) {
            $count_countryCodes++;
            $spreadsheet->setActiveSheetIndex(1);
            $spreadsheet->getActiveSheet()->SetCellValue("A" . $count_countryCodes, ucwords(strtolower($item->{trans('user.name')})));
        }


        $count_branches = 0;
        foreach ($branches as $item) {
            $count_branches++;
            $spreadsheet->setActiveSheetIndex(1);
            $spreadsheet->getActiveSheet()->SetCellValue("B" . $count_branches, $item->name);
        }

        $count_position = 0;
        foreach ($position as $item) {
            $count_position++;
            $spreadsheet->setActiveSheetIndex(1);

            $spreadsheet->getActiveSheet()->SetCellValue("C" . $count_position, $item->name);
        }

        $count_profiles = 0;
        foreach ($profiles as $item) {
            $count_profiles++;
            $spreadsheet->setActiveSheetIndex(1);
            $spreadsheet->getActiveSheet()->SetCellValue("D" . $count_profiles, $item->name);
        }

        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];

        $spreadsheet->setActiveSheetIndex(0);
        $spreadsheet->getActiveSheet()->getStyle('A1:K1')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->setCellValue('A1', 'Name')
            ->setCellValue('B1', 'Last Name')
            ->setCellValue('C1', 'Birthday')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Country')
            ->setCellValue('F1', 'Phone')
            ->setCellValue('G1', 'Can Notify')
            ->setCellValue('H1', 'Branch')
            ->setCellValue('I1', 'Position')
            ->setCellValue('J1', 'Session type')
            ->setCellValue('K1', 'Profile');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(13); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(13); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(18); //->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(15); //->setAutoSize(true);

        for ($i = 2; $i <= 100; $i++) {

            $validation = $spreadsheet->getActiveSheet()->getCell('C' . $i)->getDataValidation();
            $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_DATE);
            $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
            $validation->setAllowBlank(true);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Only Dates are allowed!');
            $validation->setPromptTitle('Allowed input');
            $validation->setPrompt('Only Dates are allowed (Pref. YYYY-MM-DD).');


            if ($count_countryCodes > 0) {
                $validation = $spreadsheet->getActiveSheet()->getCell('E' . $i)->getDataValidation();
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Select the user country.');
                $validation->setFormula1('=Sheet1!$A$1:$A$' . $count_countryCodes);
            }


            $validation = $spreadsheet->getActiveSheet()->getCell('F' . $i)->getDataValidation();
            $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_WHOLE);
            $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_STOP);
            $validation->setAllowBlank(true);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Only numbers are allowed!');
            $validation->setPromptTitle('Allowed input');
            $validation->setPrompt('Only numbers are allowed.');
            $validation->setFormula1(1111111111);
            $validation->setFormula2(9999999999);

            $validation = $spreadsheet->getActiveSheet()->getCell('G' . $i)->getDataValidation();
            $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
            $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Select if the user can send notifications.');
            $validation->setFormula1('"Yes,No"');

            if ($count_branches > 0) {
                $validation = $spreadsheet->getActiveSheet()->getCell('H' . $i)->getDataValidation();
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Select the user Branch.');
                $validation->setFormula1('=Sheet1!$B$1:$B$' . $count_branches);
            }

            if ($count_position > 0) {
                $validation = $spreadsheet->getActiveSheet()->getCell('i' . $i)->getDataValidation();
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Select the user Position.');
                $validation->setFormula1('=Sheet1!$C$1:$C$' . $count_position);
            }


            $validation = $spreadsheet->getActiveSheet()->getCell('J' . $i)->getDataValidation();
            $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
            $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Pick from list');
            $validation->setPrompt('Select if the user will have access to the session by user and password or social login.');
            $validation->setFormula1('"Social,User and password"'); // Make sure to put the list items between " and " if your list is simply a comma-separated list of values !!!

            if ($count_profiles > 0) {
                $validation = $spreadsheet->getActiveSheet()->getCell('K' . $i)->getDataValidation();
                $validation->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
                $validation->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
                $validation->setAllowBlank(false);
                $validation->setShowInputMessage(true);
                $validation->setShowErrorMessage(true);
                $validation->setShowDropDown(true);
                $validation->setErrorTitle('Input error');
                $validation->setError('Value is not in list.');
                $validation->setPromptTitle('Pick from list');
                $validation->setPrompt('Select the user Profile.');
                $validation->setFormula1('=Sheet1!$D$1:$D$' . $count_profiles);
            }
        }

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_end_clean();
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="file.xls"');
        $writer->save('php://output');
        exit;

    }


    public function profile_image(Request $request, User $user)
    {
        $data = $request->validate([
            "file" => "required",
            ]);

            $new_name = time() . '.' . $request->file->extension();
            $path = $request->file->storeAs('profile_images', $new_name, 'public');

            $user->avatar =  $path;
            $user->save();
            $return["user"] = $user;
            return response()->json($user);
    }

    public function send_push()
    {
            $parameters = array(
                "include_player_ids" => array("3ffe8c62-9897-4982-b067-a6719f2f1a04", "48cd965d-2947-4d0a-a449-ae9b0419aaea", "54958d9a-e12f-4d57-9d65-7d1c58a67368"),
                //"template_id" => "d43f202b-838e-4aef-90c8-307b03e9c121",
                //"tags" => array("name"=>"Carlos Andres"),
                "app_id" => "b5baa53e-d6a0-4752-be32-b36eede1b087",
                "contents" => array("en" => "See posts.", "es" => "Ver publicaciones."),
                "headings" => array("en" => "Juan commented on your post.", "es" => "Juan ha comentado en tu publicación."),
                "subtitle" => array("en" => "Juan commented on your post.", "es" => "Juan ha comentado en tu publicación."),
                "android_led_color" => "FF9900FF",
                "test_type" => "1",
                "data" => array("section" => "community", "item_id" => "1")
            );

            $message =  new One(env("OS_APP_ID"), env("OS_REST_API"), env("OS_AUTH"));
            $message->sendNotificationCustom($parameters);
    }

    public function make_bitly_url($url)
    {

            $bitly = 'http://api.bit.ly/shorten?version=2.0.1&amp;longUrl=' . urlencode($url) . '&amp;login=inmovsas&amp;apiKey=R_8eb5cc2d6d174a41bcb45eef81a3ab4a&amp;format=json';
            $response = file_get_contents($bitly);
            $json = json_decode($response, true);
            return $json['results'][$url]['shortUrl'];
    }

}
