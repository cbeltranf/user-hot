<?php

namespace Inmovsoftware\UserApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes;
    protected $table = "it_countries";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name','code','iso','nicename','iso3','numcode','phonecode'];

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".name", 'like', "%$param%");
        $query->orwhere($this->table. ".code", 'like', "%$param%");
        $query->orwhere($this->table. ".nicename", 'like', "%$param%");
        $query->orwhere($this->table. ".phonecode", 'like', "%$param%");
    }

    public function Users()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'it_countries_id', 'id');

    }


}
