<?php

namespace Inmovsoftware\UserApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Group extends Model
{
    use SoftDeletes;
    protected $table = "it_groups_users";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'description', 'it_business_id', 'status', 'parameters',  'created_at',  'upated_at', 'deleted_at'];


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".name", 'like', "%$param%");
    }

    public function Groupsusers()
    {
        return $this->hasMany('Inmovsoftware\UserApi\Models\V1\Groupuser', 'it_groups_users_id', 'id');

    }
}
