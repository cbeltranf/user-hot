<?php

namespace Inmovsoftware\UserApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Groupuser extends Model
{
    use SoftDeletes;
    protected $table = "it_user_group";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['it_users_id','it_groups_users_id'];


    public function User()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\User', 'it_users_id', 'id');

    }


    public function Group()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\Group', 'it_groups_users_id', 'id');

    }
}
