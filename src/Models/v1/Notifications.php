<?php

namespace Inmovsoftware\UserApi\Models\V1;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "it_notifications";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $hidden = [
        'password',
    ];
    protected $dates = ['deleted_at'];
    protected $fillable = ['id','it_business_id','title','message','datesend','from_to_value'];


    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".title", 'like', "%$param%");
        $query->orwhere($this->table. ".message", 'like', "%$param%");
    }

}
