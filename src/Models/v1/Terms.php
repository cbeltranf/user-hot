<?php

namespace Inmovsoftware\UserApi\Models\V1;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class Terms extends Model
{
    use SoftDeletes;
    use \Inmovsoftware\GeneralApi\Traits\V1\helper;

    protected $table = "it_terms_conditions_policies";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $dates = ['deleted_at','create_date', 'cancel_date'];
    protected $fillable = ['terms_en','terms_es', 'policy_en','policy_es', 'create_date', 'cancel_date',  'status'];

    public function scopefilterValue($query, $param)
    {
        $query->orwhere($this->table. ".term_and_conditions", 'like', "%$param%");
        $query->orwhere($this->table. ".policy_data", 'like', "%$param%");
    }

    public function UsersTermsAccepted()
    {

        return $this->belongsToMany('Inmovsoftware\UserApi\Models\V1\User', 'it_user_term_condition_policy', 'it_terms_conditions_policies_id', 'it_users_id');
    }

}
