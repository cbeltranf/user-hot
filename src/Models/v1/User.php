<?php

namespace Inmovsoftware\UserApi\Models\V1;

use Inmovsoftware\PositionApi\Models\V1\Position;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use DB;

class User extends Model
{
    use SoftDeletes;
    protected $table = "it_users";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];
    protected $fillable = ['password', 'it_branches_id', 'email',  'login_alt', 'name', 'last_name', 'country_phone', 'it_countries_id',  'phone', 'birthday', 'avatar_path', 'avatar', 'it_groups_users_id', 'it_profile_id', 'languaje', 'first_login', 'update_data', 'last_login', 'platform', 'model', 'version', 'uuid', 'token', 'date_password_change', 'recovery_code', 'it_positions_id', 'session_type', 'can_notify'];
    protected $appends = ['groups', 'position'];


    public function Profile()
    {
        return $this->belongsTo('Inmovsoftware\ProfileApi\Models\V1\Profile', '', 'id');
    }

    public function scopeselectAllItems($query)
    {
        $query->select(
            $this->table . ".id",
            $this->table . ".it_branches_id",
            $this->table . ".email",
            $this->table . ".login_alt",
            $this->table . ".name",
            $this->table . ".last_name",
            $this->table . ".can_notify",
            $this->table . ".update_data",
            $this->table . ".phone",
            $this->table . ".country_phone",
            $this->table . ".it_countries_id",
            $this->table . ".birthday",
            $this->table . ".session_type",
            $this->table . ".avatar",
            $this->table . ".status",
            $this->table . ".it_profile_id",
            $this->table . ".languaje",
            $this->table . ".first_login",
            $this->table . ".last_login",
            $this->table . ".platform",
            $this->table . ".model",
            $this->table . ".version",
            $this->table . ".uuid",
            $this->table . ".token",
            $this->table . ".date_password_change",
            $this->table . ".recovery_code",
            $this->table . ".deleted_at",
            $this->table . ".it_positions_id"
        );
    }

    public function scopefilterValue($query, $param)
    {

        $query->join('it_positions', 'it_positions.id', '=', $this->table . '.it_positions_id');
        $query->orwhere($this->table . ".name", 'like', "%$param%");
        $query->orwhere($this->table . ".last_name", 'like', "%$param%");
        $query->orwhere($this->table . ".email", 'like', "%$param%");
        $query->orwhere($this->table . ".phone", 'like', "%$param%");
        $query->orwhere(DB::raw('CONCAT(' . $this->table . '.name' . ",' '," . $this->table . ".last_name" . ')'), 'like', "%$param%");
        $query->orwhere("it_positions.name", 'like', "%$param%");
    }

    public static function scopethisUser($query)
    {
        $query->it_users = auth()->user()->id;
    }

    public function User()
    {
        return $this->hasMany('Inmovsoftware\UserApi\Models\V1\Groupuser', 'it_users_id', 'id');
    }

    public function Country()
    {
        return $this->belongsTo('Inmovsoftware\UserApi\Models\V1\Country', 'it_countries_id', 'id');
    }

    public function TermsAccepted()
    {
        return $this->belongsToMany('Inmovsoftware\UserApi\Models\V1\Terms', 'it_user_term_condition_policy', 'it_users_id', 'it_terms_conditions_policies_id');
    }

    public function getGroupsAttribute()
    {
        $item = Groupuser::where("it_users_id", "=",  $this->id)->get();
        return  $item;
    }

    public function getPositionAttribute()
    {
        $item = Position::where("id", "=",  $this->it_positions_id)->get();
        return  $item;
    }
}
