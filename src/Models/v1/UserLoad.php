<?php

namespace Inmovsoftware\UserApi\Models\V1;

use Inmovsoftware\UserApi\Models\V1\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class UserLoad implements ToModel
{

    public function model(array $row)
    {
        return new User([
            'name'  => $filas[0],
            'last_name'  => $filas[1],
            'birthday'  => $filas[2],
            'email'  => $filas[3],
            'country_phone'  => $filas[4],
            'phone'  => $filas[5],
            'can_notify'  => $filas[6],
            'it_branches_id'  => $filas[7],
            'it_positions_id'  => $filas[8],
            'session_type' => $filas[9],
            'it_profile_id'  => $filas[10],
            'password'  => Hash::make(12345678),
        ]);
    }

}
