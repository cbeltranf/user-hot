<?php

namespace Inmovsoftware\UserApi\Models\V1;
use Illuminate\Database\Eloquent\Model;

class Userloginview extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "vs_login";
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $hidden = [
        'password',
    ];
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'last_name',  'phone','email', 'it_business_id', 'avatar_path', 'it_groups_users_id', 'it_profile_id', 'languaje', 'uuid', 'first_login', 'last_login', 'session_type'];

}
