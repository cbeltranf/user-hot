<?php

namespace Inmovsoftware\UserApi\Providers;

use Illuminate\Support\ServiceProvider;
use Inmovsoftware\UserApi\Http\Resources\V1\GlobalCollection;
use Illuminate\Support\Facades\Artisan;

class InmovTechUserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');



        $this->publishes([
            __DIR__.'/../../resources/lang' => resource_path('/lang'),
                ], 'userLangs');

        Artisan::call('vendor:publish' , [
                      '--tag' => 'userLangs',
                    '--force' => true,
        ]);

        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'user');



    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $env_update = $this->changeEnv([
            'OS_APP_ID'   => 'b5baa53e-d6a0-4752-be32-b36eede1b087',
            'OS_REST_API'       => 'NTczNjY4NTYtNDdkZS00YTJjLThiOWEtN2FmYzYyYWQyZGE3']);

        $this->app->register('Maatwebsite\Excel\ExcelServiceProvider');
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('Excel', 'Maatwebsite\Excel\Facades\Excel');


        $this->app->make('Inmovsoftware\UserApi\Models\V1\User');
        $this->app->make('Inmovsoftware\UserApi\Models\V1\Terms');
        $this->app->make('Inmovsoftware\UserApi\Models\V1\Country');
        $this->app->make('Inmovsoftware\UserApi\Models\V1\Group');
        $this->app->make('Inmovsoftware\UserApi\Models\V1\Groupuser');
        $this->app->make('Inmovsoftware\UserApi\Models\V1\Notifications');
        $this->app->make('Inmovsoftware\UserApi\Models\V1\Userloginview');
        $this->app->make('Inmovsoftware\UserApi\Http\Controllers\V1\CountriesController');
        $this->app->make('Inmovsoftware\UserApi\Http\Controllers\V1\UserController');
        $this->app->make('Inmovsoftware\UserApi\Http\Controllers\V1\GroupsController');
        $this->app->make('Inmovsoftware\UserApi\Http\Controllers\V1\NotificationsController');
        $this->registerHandler();
    }


    protected function registerHandler()
    {
        \App::singleton(
            Illuminate\Http\Resources\Json\ResourceCollection::class,
            GlobalCollection::class
        );

    }



    protected function changeEnv($data = array()){
        if(count($data) > 0){

            // Read .env-file
            $env = file_get_contents(base_path() . '/.env');

            // Split string on every " " and write into array
            $env = preg_split('/\s+/', $env);
/*
            Log::error(
                'ERR data ' .print_r($data, true)
            );
            Log::error(
                'ERR env ' .print_r($env, true )
            );
*/
            $temp ='';
            foreach((array)$data as $key => $value){
                foreach($env as $env_key => $env_value){
                    if(!empty(trim($env_value))){
                    $temp = explode("=", $env_value, 2);
                        $to_analyze[$temp[0]] = $temp[1];
                    }

                }
            }

/*            Log::error(
                'To analyze  ' .print_r($to_analyze, true )
            );

*/
            // Loop through given data
            foreach((array)$data as $key => $value){

                if (! array_key_exists($key, $to_analyze)) {
                    $env[$key] = $key . "=" . $value;
                                    }

                // Loop through .env-data
                foreach($env as $env_key => $env_value){


                    // Turn the value into an array and stop after the first split
                    // So it's not possible to split e.g. the App-Key by accident
                    $entry = explode("=", $env_value, 2);

                    // Check, if new key fits the actual .env-key
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }

            // Turn the array back to an String
            $env = implode("\n", $env);

            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            return true;
        } else {
            return false;
        }
    }


}
