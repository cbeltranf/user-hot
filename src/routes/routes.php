<?php
use Illuminate\Http\Request;

Route::middleware(['api'])->group(function () {
    Route::group([
        'prefix' => 'api/v1'
    ], function () {
        Route::get('terms/get/active', 'Inmovsoftware\UserApi\Http\Controllers\V1\TermsController@get_active');
        Route::get('get/users/format/{id}', 'Inmovsoftware\UserApi\Http\Controllers\V1\UserController@generate_custom_excel');
    });
});



Route::middleware(['api', 'jwt'])->group(function () {
    Route::group([
        'prefix' => 'api/v1'
    ], function () {
        Route::apiResource('user', 'Inmovsoftware\UserApi\Http\Controllers\V1\UserController');
        Route::apiResource('groups', 'Inmovsoftware\UserApi\Http\Controllers\V1\GroupsController');
        Route::apiResource('notifications', 'Inmovsoftware\UserApi\Http\Controllers\V1\NotificationsController');
        Route::apiResource('terms', 'Inmovsoftware\UserApi\Http\Controllers\V1\TermsController');
        Route::get('terms/check/accept', 'Inmovsoftware\UserApi\Http\Controllers\V1\TermsController@check_accept');
        Route::get('terms/accept/new', 'Inmovsoftware\UserApi\Http\Controllers\V1\TermsController@accept_again');

        Route::post('user/profile_image/update/{user}', 'Inmovsoftware\UserApi\Http\Controllers\V1\UserController@profile_image');
        Route::get('global/flags', 'Inmovsoftware\UserApi\Http\Controllers\V1\CountriesController@index_select');
        Route::post('users/store/mass', 'Inmovsoftware\UserApi\Http\Controllers\V1\UserController@mass_store');
        Route::post('user/switch/status/{id}', 'Inmovsoftware\UserApi\Http\Controllers\V1\UserController@active_inactive');

        Route::post('groups/users/associate', 'Inmovsoftware\UserApi\Http\Controllers\V1\GroupsController@associate');
        Route::post('groups/users/{group_id}/list', 'Inmovsoftware\UserApi\Http\Controllers\V1\GroupsController@get_integrants');
    });
});
